#pragma once

#include <string>
#include <sstream>
#include <iostream>
#include <cstdint>

#include <boost/optional.hpp>

#include "HelperFunctions.hpp"
#include "Decoders.hpp"

class TsPesDissector
{
public:
    static const uint16_t TS_PACKET_SIZE = 188;
    
    TsPesDissector(const byte_ptr packet_ptr, bool debug=false)
        : m_ts_header_and_extension_size(0)
        , m_ts_payload_size(0)
        , m_pes_header_size(0)
        , m_pes_payload_chunk_size(0)
        , m_payload_offset(TsHeader::SIZE)
    {
        // Decode MPEG TS Header
        m_ts_header = TsHeader( packet_ptr );

        // Decode MPEG TS ADAPTATION FIELD if needed
        if (  m_ts_header->getAdaptationFieldControl() == TsHeader::ADAPTATION_FIELD_AND_PAYLOAD 
           || m_ts_header->getAdaptationFieldControl() == TsHeader::ADAPTATION_FIELD_ONLY )
        {
            m_ts_adaptation_field = TsAdaptationField( packet_ptr + m_payload_offset );
            m_payload_offset += m_ts_adaptation_field->getAdaptationFieldSize() + 1;
        }

        // Decode MPEG TS ADAPTATION FIELD EXTENSION if present
        // NB: it is possible to have ADAPTATION FIELD with lenght 0 so it has to be checked
        if ( m_ts_adaptation_field 
            && m_ts_adaptation_field->getAdaptationFieldSize() 
            && m_ts_adaptation_field->getAdaptafionFieldExtension() )
        {
            m_ts_adaptation_extension = TsAdaptationExtension( packet_ptr + m_payload_offset );
            m_payload_offset += m_ts_adaptation_extension->getAdaptationExtensionSize();
        }

        // Calculate TS Header & TS Payload lenghts
        m_ts_header_and_extension_size = m_payload_offset;
        m_ts_header->getAdaptationFieldControl() == TsHeader::ADAPTATION_FIELD_ONLY 
            ? m_ts_header_and_extension_size = 0 
            : m_ts_payload_size = TS_PACKET_SIZE - m_payload_offset;

        // Decode PES Header
        if (  ( m_ts_header->getAdaptationFieldControl() == TsHeader::PAYLOAD_ONLY
              || m_ts_header->getAdaptationFieldControl() == TsHeader::ADAPTATION_FIELD_AND_PAYLOAD )
           && m_ts_payload_size >= PesHeader::MINIMUM_SIZE )
        {
            auto pes_header = PesHeader( packet_ptr + m_payload_offset );
            if ( pes_header.hasValidStartCodePrefix() )
            {
                m_pes_header = PesHeader( packet_ptr + m_payload_offset );
                m_payload_offset += m_pes_header->getHeaderSize();
                m_pes_header_size = m_pes_header->getHeaderSize();
            }
        }

        // Calculate PES Payload lenght 
        // NB: PES header is not present in all TS packets since PES in semgented in multiple TS)
        m_pes_payload_chunk_size = TS_PACKET_SIZE - m_payload_offset;
    };

    std::string dump( const byte_ptr data_ptr = nullptr )
    {
        // save flags state
        auto cout_flags_state = std::cout.flags();

        if ( data_ptr )
        {
            return dumpInfo() + std::string(" - ") + dumpDataSummary(data_ptr) + std::string("\n");
        }
        else
        {
            return dumpInfo();
        }
        
        // restore flags state
        std::cout.flags(cout_flags_state);
    }

    // Minimum sanity check
    bool isValid()
    {
        if ( !(m_ts_payload_size <= TS_PACKET_SIZE - TsHeader::SIZE) ) return false;

        if ( !(m_pes_payload_chunk_size <= TS_PACKET_SIZE - TsHeader::SIZE) ) return false;

        if ( !(m_payload_offset < TS_PACKET_SIZE) ) return false;

        if ( !(m_ts_header->getSyncByte() == TsHeader::SYNC_PATTERN) ) return false;

        if ( m_pes_header && !(m_pes_header->getStartCodePrefix() == PesHeader::START_CODE_PREFIX) ) return false;

        return true;
    }

public:
    uint16_t m_ts_header_and_extension_size;
    uint16_t m_ts_payload_size;
    uint16_t m_pes_header_size;
    uint16_t m_pes_payload_chunk_size;
    uint16_t m_payload_offset;
    optional<TsHeader> m_ts_header;
    optional<TsAdaptationField> m_ts_adaptation_field;
    optional<TsAdaptationExtension> m_ts_adaptation_extension;
    optional<PesHeader> m_pes_header;

private:
    std::string dumpInfo()
    {
        std::stringstream output{};

        std::string stream_type("chunk");
        uint16_t pes_packet_lenght(0);
        if ( m_pes_header )
        {
            stream_type = m_pes_header->getStreamTypeString();
            pes_packet_lenght = m_pes_header->getPesPacketLenght();
        }

        output << "[PID: " << std::setfill('0') << std::setw(2) << m_ts_header->getPID()
               << ", Sync: " << static_cast<uint16_t>(m_ts_header->getSyncByte())
               << ", Err: " << m_ts_header->getTransportErrorIndicator()
               << ", PUSI: " << m_ts_header->getPUSI()
               << ", TP: " << m_ts_header->getTransportPriority()
               << ", TSC: " << static_cast<uint16_t>(m_ts_header->getTSC())
               << ", AFC: " << static_cast<uint16_t>(m_ts_header->getAdaptationFieldControl())
               << ", CC: " << static_cast<uint16_t>(m_ts_header->getContinuityCounter())
               << ", TsLen: (" << std::setfill('0') << std::setw(3) << m_ts_header_and_extension_size
               << ", " << std::setfill('0') << std::setw(3) << m_ts_payload_size << ")"
               << ", PesHdrLen: " << std::setfill('0') << std::setw(3) << m_pes_header_size
               << ", PesChunkLen: " << std::setfill('0') << std::setw(3) << m_pes_payload_chunk_size
               << ", PesPL: " << std::setfill('0') << std::setw(4) << pes_packet_lenght
               << ", " << stream_type
               << "]";

        return output.str();
    }

    std::string dumpDataSummary( const byte_ptr data_ptr )
    {
        std::stringstream output{};

        output << "{" << dumpBytes( data_ptr, 4 ) << " ... ";

        if ( m_pes_header_size )
        {
            // PES header and PES payload start
            output << "{" << dumpBytes(data_ptr + m_ts_header_and_extension_size, 4) 
                   << " ... "
                   << "[" << dumpBytes(data_ptr + m_ts_header_and_extension_size + m_pes_header_size, 4)
                   << " ... ";
        }
        else
        {
            // TS payload start
            output << "[" << dumpBytes(data_ptr + m_ts_header_and_extension_size, 4) 
                   << " ... ";
        }
        
        output << dumpBytes(data_ptr + TS_PACKET_SIZE - 4, 4) << "]";
    
        return output.str();
    }
};
