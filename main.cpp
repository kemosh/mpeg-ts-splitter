#include <iostream>
#include <string>
#include <boost/program_options.hpp>

#include "MpegTsPacketsProcessor.hpp"

namespace po = boost::program_options;

int main (int argc, char *argv[])
{
   std::string input_filename;
   uint64_t limit_processed_packets;
   uint16_t pid_filter;
   bool verbose;

   po::options_description desc("Usage:\n  mpeg-ts_splitter -i input_filename [OPTIONS]\nAllowed options");
	desc.add_options()
		("help", "produce help message")
      ("input,i", po::value<std::string>(&input_filename)
         , "MPEG-TS input filename")
      ("number_of_packets,n", po::value<uint64_t>(&limit_processed_packets)->default_value(0)
         ,"Limit number of processed packets, default of 0 means no limit.")
      ("pid_filter,p", po::value<uint16_t>(&pid_filter)->default_value(0)
         ,"Process only specified PID. Default value of 0 will process all PIDs.")
      ("verbose,v", po::bool_switch(&verbose)->default_value(false)
         ,"Show info for each TS packet. It will include a summary of HEX dump.");

   // Parse
   po::variables_map vm;
   try
   {
      po::store(po::parse_command_line(argc, argv, desc), vm);
      po::notify(vm);    
   }
   catch(const std::exception& e)
   {
      std::cout << "Wrong or unrecognized option(s)! Use --help for help." << std::endl;
      return 1;
   }

   if ( vm.size() == 0 || vm.count("help") )
   {
      std::cout << desc << "\n";
      return 0;
   }

   if ( !vm.count("input") )
   {
      std::cout << "Error, missing mandatory parameter! Run: mpeg-ts_splitter --help" << std::endl;
      return 1;
   }

   if ( !fileExists(input_filename) )
   {
      std::cout << "Error, file not found!" << std::endl;
      return 1;
   }

   std::cout << "Input file: " << input_filename << std::endl;
   
   // Create processor and init
   MpegTsPacketsProcessor my_packets_processor(input_filename, limit_processed_packets, pid_filter, verbose);
   std::cout << "Input file size: " << my_packets_processor.getFileSizeMb() << " MB" << std::endl;
   std::cout << "Input number of MPEG-TS packets: " << my_packets_processor.getNumberOfPackets() << std::endl;

   // Do processing
   my_packets_processor.process();
   my_packets_processor.dumpSummary();

   return 0; 
}
