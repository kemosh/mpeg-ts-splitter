# MPEG-TS Splitter
`mpeg-ts_splitter` is a command line tool capable to extract **AUDIO** and **VIDEO** elementary streams from a MPEG-TS file. The tool will parse the provided file, reassemble [PES packets](https://en.wikipedia.org/wiki/Packetized_elementary_stream) from fixed size [TS packets](https://en.wikipedia.org/wiki/MPEG_transport_stream) and, for each AUDIO/VIDEO PID, write PES payload to file.

## Installation
Check following requirements:
 * git
 * git-lfs (used for test files)
 * cmake 3.10+
 * boost (in particular `boost::program_options` and `boost::optional`)
 * gcc (**C++14** support required)

If requirements are satisfied, istallation steps are:
 * `git clone https://gitlab.com/kemosh/mpeg-ts-splitter.git`
 * `cd mpeg-ts-splitter`
 * `mkdir build; cd build`
 * `cmake ..; make`

## Usage
Run `mpeg-ts_splitter --help` for help:

```bash
Usage:
  mpeg-ts_splitter -i input_filename [OPTIONS]
Allowed options:
  --help                              produce help message
  -i [ --input ] arg                  MPEG-TS input filename
  -n [ --number_of_packets ] arg (=0) Limit number of processed packets, 
                                      default of 0 means no limit.
  -p [ --pid_filter ] arg (=0)        Process only specified PID. Default value
                                      of 0 will process all PIDs.
  -v [ --verbose ]                    Show info for each TS packet. It will 
                                      include a summary of HEX dump.
```

### Example of typical usage
```bash
mpeg-ts_splitter -i input_file.ts
```

Output:
```bash
Input file: ../test/elephants.ts
Input file size: 43.9169 MB
Input number of MPEG-TS packets: 244948
Stream PID: 00, Type: 0, Packets: 82, DumpFile: none, Written: 0 MB
Stream PID: 32, Type: 0, Packets: 82, DumpFile: none, Written: 0 MB
Stream PID: 33, Type: 2, Packets: 186640, DumpFile: 33_VIDEO_stream, Written: 30.9287 MB
Stream PID: 34, Type: 1, Packets: 58144, DumpFile: 34_AUDIO_stream, Written: 9.71051 MB
Total processed MPEG-TS packets: 244948
Valid MPEG-TS packets: 244948
Corrupted MPEG-TS packets: 0
```

### Example of dumping verbose info
```bash
mpeg-ts_splitter -i input_file.ts -p 1000 -v -p 34
```

First 1000 TS packets will be processed, filtering on **PID==34**. Output:

```bash
Input file: ../test/elephants.ts
Input file size: 43.9169 MB
Input number of MPEG-TS packets: 244948
#00000009 OK [PID: 34, Sync: 71, Err: 0, PUSI: 1, TP: 0, TSC: 0, AFC: 3, CC: 0, TsLen: (006, 182), PesHdrLen: 014, PesChunkLen: 168, PesPL: 2805, AUDIO] - {47 40 22 30 ... {00 00 01 c0 ... [ff f1 4c 80 ... 80 02 1f fc]
#00000019 OK [PID: 34, Sync: 71, Err: 0, PUSI: 0, TP: 0, TSC: 0, AFC: 1, CC: 0, TsLen: (004, 184), PesHdrLen: 000, PesChunkLen: 184, PesPL: 0000, chunk] - {47 00 22 11 ... [21 00 49 90 ... 02 19 00 23]
#00000042 OK [PID: 34, Sync: 71, Err: 0, PUSI: 0, TP: 0, TSC: 0, AFC: 1, CC: 0, TsLen: (004, 184), PesHdrLen: 000, PesChunkLen: 184, PesPL: 0000, chunk] - {47 00 22 12 ... [80 ff f1 4c ... 9b ed 7f 9d]
...
Stream PID: 34, Type: 1, Packets: 78, DumpFile: 34_AUDIO_stream, Written: 9.71051 MB
Total processed MPEG-TS packets: 78
Filtering on PID: 34
Valid MPEG-TS packets: 1000
Corrupted MPEG-TS packets: 0
```

To be noticed that verbose dump contains also a data summary (first bytes of every TS/PES section) at the end of the line. For instance for TS packet `#00000009` (see output sample below) data summary is:

```bash
{47 40 22 30 ... {00 00 01 c0 ... [ff f1 4c 80 ... 80 02 1f fc]
```

so we have TS header on the first "**{**", PES header on the second "**{**", payload start ("**[**") and payload end ("**]**"). PES header is present when TS packet contains the first part of segmented PES, in other cases it is just a chunk.

## Architecture
```mermaid
graph LR;
classDef blank fill:#FFFFFF,stroke:#000,stroke-width:.5px;
classDef Blu fill:#00BFFF,stroke:#000,stroke-width:.5px;
classDef Yellow fill:#F4A460,stroke:#000,stroke-width:.5px;
classDef Violet fill:#DDA0DD,stroke:#000,stroke-width:.5px;
classDef VioletLight fill:#D8BFD8,stroke:#000,stroke-width:.5px;
classDef Red fill:#CD5C5C,stroke:#000,stroke-width:.5px;
subgraph main[ ]
subgraph abstract[ ]
A1[PacketsProcessorAbstract<PACKET_DISSECTOR, PACKET_SIZE>]
end
subgraph concrete[ ]
C1[MpegTsPacketsProcessor<TsPesDissector, TS_PACKET_SIZE>] -.-> C2[TsPesDissector]
end
C1 ==> A1;
C2 -.-> C3[Decoders]
end
style main fill:#FFF,stroke:#000,stroke-width:0px;
style abstract fill:#FFF,stroke:#000,stroke-width:0px;
style concrete fill:#FFF,stroke:#000,stroke-width:0px;
class A1 VioletLight
class C1 Violet
class C2 Yellow
class C3 Red
```
In brief the architecture is made of:
 * `PacketsProcessorAbstract<PACKET_DISSECTOR, PACKET_SIZE>`: abtract class capable to load a file made of fixed size packets, iterate on it, run PACKET_DISSECTOR and call following processing methods implemented in concrete class:
    ```cpp
    private:
        virtual void init() {}
        virtual bool processPacket( PACKET_DISSECTOR& packet_dissector, const byte_ptr packet_ptr ) = 0;
        virtual void reset() {}
    ```
    At this stage **input file in fully loaded in the heap**, future improvements should avoid to load the full file (i.e. using boost memory mapping).
 * `MpegTsPacketsProcessor<TsPesDissector, TsPesDissector::TS_PACKET_SIZE>` is the concrete class implementing processing.
 * `TsPesDissector`: implements the logic to process packets. It uses decoder classes to access the single fields of each protocol layer.
 * `TsHeader, TsAdaptationField, TsAdaptationFieldExtension, PesHeader`: are the **decoders**. Each decoder uses `getField` template function to limit conditional logic and be more readable, so for instance in **TsHeader** we have following accessors:
   ```cpp
   uint8_t  getSyncByte()                { return getField<uint32_t, 0xff000000, 24, uint8_t>                 (&m_data); }
   bool     getTransportErrorIndicator() { return getField<uint32_t, 0x00800000, 23, bool>                    (&m_data); }
   bool     getPUSI()                    { return getField<uint32_t, 0x00400000, 22, bool>                    (&m_data); }
   bool     getTransportPriority()       { return getField<uint32_t, 0x00200000, 21, bool>                    (&m_data); }
   uint16_t getPID()                     { return getField<uint32_t, 0x001fff00,  8, uint16_t>                (&m_data); }
   uint8_t  getTSC()                     { return getField<uint32_t, 0x000000c0,  6, uint8_t>                 (&m_data); }
   uint8_t  getAdaptationFieldControl()  { return getField<uint32_t, 0x00000030,  4, AdaptationFieldControl>  (&m_data); }
   uint8_t  getContinuityCounter()       { return getField<uint32_t, 0x001fff00,  0, uint8_t>                 (&m_data); }
   ```
## Testing
Testing has been performed using the provided `elephant.ts` file. The two output files (`33_VIDEO_stream` and `34_AUDIO_stream`) have been checked using **ffplay** and **dvbsnoop**. In case of dvbsnoop an hex comparison with dumped PES payload has been done using following command options:

 * `dvbsnoop -s pes -nohexdumpbuffer -n 5 -if elephants.ts pid_number_33_or_34`
 * `dvbsnoop -s pes -b -n 5 -if elephants.ts pid_number_33_or_34 > output.bin`

To be notice that **VLC** is able to play the audio stream but **is not able to play the video stream** and that using 3rd party tool (i.e. **tsMuxeR**) will produce video stream that **is not byte by byte comparable**. 

## Reference
 * [MPEG Transport wiki](https://en.wikipedia.org/wiki/MPEG_transport_stream)
 * [Packetized Elementary Stream wiki](https://en.wikipedia.org/wiki/Packetized_elementary_stream)
 * [TsDuck MPEG-TS Introduction](https://tsduck.io/download/docs/mpegts-introduction.pdf)
 * [Transport Stream Science Direct](https://www.sciencedirect.com/topics/computer-science/transport-stream)
