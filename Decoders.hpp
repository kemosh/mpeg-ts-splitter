#pragma once

#include <cstdint>

#include "HelperFunctions.hpp"

// *******************************************************
// * MPEG-TS Header                                      *
// *******************************************************
class TsHeader
{
public:
    enum AdaptationFieldControl: uint8_t { PAYLOAD_ONLY=1, ADAPTATION_FIELD_ONLY=2, ADAPTATION_FIELD_AND_PAYLOAD=3 };
    static const uint16_t SIZE = 4;
    static const uint16_t SYNC_PATTERN = 0x47;
    
    TsHeader( const byte_ptr data_ptr )
    {
        // Endian swap and store header
        m_data = endianSwap( *reinterpret_cast<uint32_t*>(data_ptr) );
    }

    // Accessors
    uint8_t  getSyncByte()                { return getField<uint32_t, 0xff000000, 24, uint8_t>                 (&m_data); }
    bool     getTransportErrorIndicator() { return getField<uint32_t, 0x00800000, 23, bool>                    (&m_data); }
    bool     getPUSI()                    { return getField<uint32_t, 0x00400000, 22, bool>                    (&m_data); }
    bool     getTransportPriority()       { return getField<uint32_t, 0x00200000, 21, bool>                    (&m_data); }
    uint16_t getPID()                     { return getField<uint32_t, 0x001fff00,  8, uint16_t>                (&m_data); }
    uint8_t  getTSC()                     { return getField<uint32_t, 0x000000c0,  6, uint8_t>                 (&m_data); }
    uint8_t  getAdaptationFieldControl()  { return getField<uint32_t, 0x00000030,  4, AdaptationFieldControl>  (&m_data); }
    uint8_t  getContinuityCounter()       { return getField<uint32_t, 0x001fff00,  0, uint8_t>                 (&m_data); }

private:
    uint32_t m_data;
};

// *******************************************************
// * MPEG-TS Adaptation Field                            *
// *******************************************************
class TsAdaptationField
{
public:
    TsAdaptationField( const byte_ptr data_ptr )
    {
        m_data = endianSwap( *reinterpret_cast<uint16_t*>(data_ptr) );
    }

    // Accessors
    uint8_t getAdaptationFieldSize()    { return getField<uint16_t, 0xff00, 8, uint8_t> (&m_data); }
    bool    getAdaptafionFieldExtension() { return getField<uint16_t, 0x0001, 0, bool>    (&m_data); }

private:
    uint16_t m_data;
};

// *******************************************************
// * MPEG-TS Adaptation Field Extension                  *
// *******************************************************
class TsAdaptationExtension
{
public:
    TsAdaptationExtension( const byte_ptr data_ptr )
    {
        m_data = endianSwap( *reinterpret_cast<uint16_t*>(data_ptr) );
    }

    // Accessors
    uint8_t getAdaptationExtensionSize() { return getField<uint16_t, 0xff00, 8, uint8_t> (&m_data); }

private:
    uint16_t m_data;
};

// *******************************************************
// * PES Header                                          *
// *******************************************************
class PesHeader
{
public:
    enum StreamType: uint8_t { OTHER=0, AUDIO, VIDEO };
    static const uint16_t MINIMUM_SIZE = 9;
    static const uint32_t START_CODE_PREFIX = 0x000001;

    PesHeader( const byte_ptr data_ptr )
    {
        m_data = endianSwap( *reinterpret_cast<uint64_t*>(data_ptr) );
        m_pes_header_variable_lenght = *(data_ptr + 8);
    }

    // Accessors
    uint32_t getStartCodePrefix() { return getField<uint64_t, 0xffffff0000000000, 40, uint32_t>  (&m_data); }
    uint8_t  getStreamId()        { return getField<uint64_t, 0x000000ff00000000, 32, uint8_t>   (&m_data); }
    uint16_t getPesPacketLenght() { return getField<uint64_t, 0x00000000ffff0000, 16, uint16_t>  (&m_data); }
    uint8_t  getHeaderSize()      { return m_pes_header_variable_lenght + 9; }

    bool hasValidStartCodePrefix() { return getStartCodePrefix() == START_CODE_PREFIX; }

    StreamType getStreamType() 
    {
        if ( getStreamId() >= 0xC0 && getStreamId() <= 0xDF ) return AUDIO;
        else if ( getStreamId() >= 0xE0 && getStreamId() <= 0xEF ) return VIDEO;
        else return OTHER;
    }

    std::string getStreamTypeString()
    {
        if ( getStreamType() == PesHeader::AUDIO ) return std::string("AUDIO");
        else if ( getStreamType() == PesHeader::VIDEO ) return std::string("VIDEO");
        else return std::string("OTHER");
    }

private:
    uint64_t m_data;
    uint8_t  m_pes_header_variable_lenght;
};
