#pragma once

#include <fstream>
#include <iostream>
#include <iomanip>
#include <utility>
#include <algorithm>
#include <cstdint>
#include <memory>
#include <vector>
#include <string>

#include "HelperFunctions.hpp"

template<typename PACKET_DISSECTOR, uint64_t PACKET_SIZE>
class PacketsProcessorAbstract
{
public:
    PacketsProcessorAbstract( const std::string& filename, uint64_t limit_processed_packets=0 )
        : m_number_of_packets(0)
        , m_file_size_mb(0)
        , m_limit_processed_packets(limit_processed_packets)
        , m_processed_packets_counter(0)
        , m_corrupted_packets_counter(0)
    {
        loadBinaryFile(filename);

        // calculate number of packets
        //assert(m_buffer->size() % PACKET_SIZE == 0);
        m_number_of_packets = m_buffer->size() / PACKET_SIZE;

        // calculate file size MByte
        m_file_size_mb = m_buffer->size() / 1024.0 / 1024.0;
    }

    // Virtual DTOR
    virtual ~PacketsProcessorAbstract() {}

    uint64_t getNumberOfPackets() { return m_number_of_packets; }

    float getFileSizeMb() { return m_file_size_mb; }

    uint64_t getNumberOfProcessedPackets() { return m_processed_packets_counter; }

    uint64_t getNumberOfCorruptedPackets() { return m_corrupted_packets_counter; }

    void process()
    {
        init();

        uint64_t max_buffer_offset; 
        m_limit_processed_packets 
            ? max_buffer_offset = std::min( static_cast<uint64_t>(m_buffer->size()), m_limit_processed_packets * PACKET_SIZE )
            : max_buffer_offset = m_buffer->size();

        for ( uint64_t buffer_offset = 0; buffer_offset < max_buffer_offset; buffer_offset += PACKET_SIZE )
        {
            auto packet_dissector = PACKET_DISSECTOR( &m_buffer->operator[](buffer_offset) );

            if ( !processPacket( packet_dissector, &m_buffer->operator[](buffer_offset) ) )
            {
                m_corrupted_packets_counter++;
            }

            m_processed_packets_counter++;
        }

        reset();
    }

private:
    // METHODS TO BE DEFINED IN DERIVED CLASS
    virtual void init() {}
    virtual bool processPacket( PACKET_DISSECTOR& packet_dissector, const byte_ptr packet_ptr ) = 0;
    virtual void reset() {}

private:
    // buffer fully allocated on the heap
    std::unique_ptr<std::vector<byte>> m_buffer;

    // parameters
    uint64_t m_number_of_packets;
    float m_file_size_mb;
    uint64_t m_limit_processed_packets;
    uint64_t m_processed_packets_counter;
    uint64_t m_corrupted_packets_counter;

    // load binary file to internal buffer
    void loadBinaryFile( const std::string& filename )
    {
        std::ifstream ifs(filename, std::ios::binary | std::ios::ate);
        std::ifstream::pos_type pos = ifs.tellg();

        // allocate buffer
        m_buffer = std::make_unique<std::vector<byte>>(pos);

        ifs.seekg(0, std::ios::beg);
        ifs.read(&m_buffer->operator[](0), pos);
    }
};
