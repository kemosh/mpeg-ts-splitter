#pragma once

#include <iostream>
#include <fstream>
#include <cstdint>
#include <string>
#include <map>

#include "PacketsProcessorAbstract.hpp"
#include "TsPesDissector.hpp"
#include "HelperFunctions.hpp"

// *******************************************************
// * Stream Descriptor used by MpegTsPacketsProcessor    *
// *******************************************************
struct StreamDescriptor
{
    StreamDescriptor()
        : m_stream_type(PesHeader::OTHER)
        , m_filename("none")
        , m_packets_counter(0)
        , m_written_bytes_counter(0)
    {
    }

    PesHeader::StreamType m_stream_type;
    std::string m_filename;
    optional<std::ofstream> m_file;
    uint64_t m_packets_counter;
    uint64_t m_written_bytes_counter;
};

// *******************************************************
// * MpegTsPacketsProcessor                              *
// *******************************************************
class MpegTsPacketsProcessor : public PacketsProcessorAbstract<TsPesDissector, TsPesDissector::TS_PACKET_SIZE>
{
public:
    MpegTsPacketsProcessor( const std::string& filename, uint64_t limit_processed_packets=0, uint16_t pid_filter=0, bool verbose=false )
        : PacketsProcessorAbstract<TsPesDissector, TsPesDissector::TS_PACKET_SIZE>(filename, limit_processed_packets)
        , m_pid_filter(pid_filter)
        , m_filtered_out_counter(0)
        , m_verbose(verbose)
    {}

    void dumpSummary()
    {
        // save flags state
        auto cout_flags_state = std::cout.flags();

        for ( auto& element : m_stream_descriptors )
        {
            std::cout << "Stream PID: " << std::setfill('0') << std::setw(2) << std::right << element.first
                      << ", Type: " << element.second.m_stream_type
                      << ", Packets: " << element.second.m_packets_counter
                      << ", DumpFile: " << element.second.m_filename
                      << ", Written: " << element.second.m_written_bytes_counter / 1024.0 / 1024.0 << " MB"
                      << std::endl;
        }
        
        std::cout << "Total processed MPEG-TS packets: " << getNumberOfProcessedPackets() - m_filtered_out_counter << std::endl;
        if ( m_pid_filter ) std::cout << "Filtering on PID: " << m_pid_filter << std::endl;
        std::cout << "Valid MPEG-TS packets: " << getNumberOfProcessedPackets() - getNumberOfCorruptedPackets() << std::endl;
        std::cout << "Corrupted MPEG-TS packets: " << getNumberOfCorruptedPackets() << std::endl;
        
        // restore flags state
        std::cout.flags(cout_flags_state);
    }

private:
    virtual bool processPacket( TsPesDissector& packet_dissector, const byte_ptr packet_ptr )
    {
        auto pid = packet_dissector.m_ts_header->getPID();

        // Apply filtering if enabled
        if ( m_pid_filter && pid != m_pid_filter )
        {
            m_filtered_out_counter++;
            return true;
        }

        // Dump verbose message if enabled
        if ( m_verbose ) dumpVerbose( packet_dissector, packet_ptr );

        // Sanity check
        if ( !packet_dissector.isValid() ) return false;

        // Create StreamDescriptor for this PID on the first PES header
        // Output to file will be enabled only for AUDIO/VIDEO streams.
        if ( !m_stream_descriptors.count(pid) )
        {
            m_stream_descriptors.emplace( pid, StreamDescriptor() );

            if ( packet_dissector.m_pes_header && packet_dissector.m_pes_header->getStreamType() != PesHeader::OTHER )
            {
                std::string filename = std::to_string(pid)
                                     + std::string("_")
                                     + packet_dissector.m_pes_header->getStreamTypeString() 
                                     + std::string("_stream");

                m_stream_descriptors[pid].m_stream_type = packet_dissector.m_pes_header->getStreamType();
                m_stream_descriptors[pid].m_filename = filename;
                m_stream_descriptors[pid].m_file = std::ofstream(filename, std::ios::out | std::ios::binary);
            }
        }

        // Write to output file stream
        m_stream_descriptors[pid].m_packets_counter++;
        if ( m_stream_descriptors[pid].m_file )
        {
            m_stream_descriptors[pid].m_file->write( packet_ptr + packet_dissector.m_payload_offset
                , TsPesDissector::TS_PACKET_SIZE - packet_dissector.m_payload_offset );

            m_stream_descriptors[pid].m_written_bytes_counter += TsPesDissector::TS_PACKET_SIZE - packet_dissector.m_payload_offset;
        }
        
        return true;
    }

    void reset()
    {
        for ( auto& element : m_stream_descriptors )
        {
            if ( element.second.m_file ) element.second.m_file->close();
        }
    }

    void dumpVerbose( TsPesDissector& packet_dissector, const byte_ptr packet_ptr )
    {
        // save flags state
        auto cout_flags_state = std::cout.flags();
        
        //<< std::right
        std::cout << "#" 
                    << std::setfill('0') << std::setw(8) << std::right << getNumberOfProcessedPackets() + 1 << " "
                    << std::vector<std::string>({"KO ", "OK "})[packet_dissector.isValid()]
                    << packet_dissector.dump(packet_ptr);

        // restore flags state
        std::cout.flags(cout_flags_state);
    }

private:
    uint16_t m_pid_filter;
    uint64_t m_filtered_out_counter;
    bool m_verbose;
    std::map<uint16_t, StreamDescriptor> m_stream_descriptors;
};