#pragma once

#include <fstream>
#include <string>
#include <sstream>
#include <cstdint>

// define types to be used as byte
using byte = char;
using byte_ptr = char*;

// define alias for boost optional
template<typename T> using optional = boost::optional<T>;

// *******************************************************
// * File Exists                                         *
// *******************************************************
bool fileExists( const std::string& filename )
{
    std::ifstream infile(filename);
    return infile.good();
}

// *******************************************************
// * ENDIAN SWAP functions                               *
// *******************************************************
inline uint8_t endianSwap(uint8_t x)
{
    return x;
}

inline uint16_t endianSwap(uint16_t x)
{
    return (x>>8) | (x<<8);
}

inline uint32_t endianSwap(uint32_t x)
{
    return (x>>24) | ((x<<8) & 0x00FF0000) | ((x>>8) & 0x0000FF00) | (x<<24);
}

inline uint64_t endianSwap(uint64_t x)
{
    return (x>>56) | ((x<<40) & 0x00FF000000000000) | ((x<<24) & 0x0000FF0000000000)
           | ((x<<8) & 0x000000FF00000000) | ((x>>8) & 0x00000000FF000000) | ((x>>24) & 0x0000000000FF0000) 
           | ((x>>40) & 0x000000000000FF00) | (x<<56);
}

// *******************************************************
// * Template Accessors                                  *
// *******************************************************
template <typename T, T BITMASK, int SHIFT, typename D, bool ENDIAN_SWAP=false>
D getField(typename std::enable_if<ENDIAN_SWAP, byte_ptr>::type data_ptr)
{
    // PERFORM ENDIAN SWAP
    auto input = endianSwap( *reinterpret_cast<T*>(data_ptr) );
    return static_cast<D>( (input & BITMASK) >> SHIFT );
}

template <typename T, T BITMASK, int SHIFT, typename D, bool ENDIAN_SWAP=false>
D getField(typename std::enable_if<!ENDIAN_SWAP, byte_ptr>::type data_ptr)
{
    auto input = *reinterpret_cast<T*>(data_ptr);
    return static_cast<D>( (input & BITMASK) >> SHIFT );
}

template <typename T, T BITMASK, int SHIFT, typename D, bool ENDIAN_SWAP=false>
D getField(typename std::enable_if<ENDIAN_SWAP, T*>::type data_ptr)
{
    // PERFORM ENDIAN SWAP
    auto input = endianSwap( *data_ptr );
    return static_cast<D>( (input & BITMASK) >> SHIFT );
}

template <typename T, T BITMASK, int SHIFT, typename D, bool ENDIAN_SWAP=false>
D getField(typename std::enable_if<!ENDIAN_SWAP, T*>::type data_ptr)
{
    auto input = *data_ptr;
    return static_cast<D>( (input & BITMASK) >> SHIFT );
}

// *******************************************************
// * Bytes dump                                          *
// *******************************************************
inline std::string dumpBytes(byte_ptr data_ptr, size_t lenght, size_t columns=16)
{
    std::stringstream output{};

    for ( size_t i = 0; i < lenght; i++ )
    {
        auto b = *(data_ptr + i);

        if ( i > 0 ) output << " ";

        output << std::setfill('0') 
               << std::setw(2) 
               << std::right 
               << std::hex 
               << (0xFF & b);

        if ( columns && i+1 % columns == 0 ) output << std::endl;
    }
    
    return output.str();
}
